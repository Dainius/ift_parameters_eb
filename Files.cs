﻿using System;

namespace FT_Parameters
{
    [Serializable]
    public class Files
    {       
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string ObjectID { get; set; }
        public string PeriodStart { get; set; }
        public string PeriodEnd { get; set; }
        public string Status { get; set; }
    }
}
