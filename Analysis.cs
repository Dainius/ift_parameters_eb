﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using ExcelDataReader;
using ClosedXML.Excel;

namespace FT_Parameters
{
    public class Analysis
    {
        public static bool FStop { get; set; }
        public string FileBatchFolder_Name { get; set; }
        public List<string> FilePairs { get; set; }
        public decimal ProgressNr { get; set; }
        public int RecTimeBase { get; set; }
        public bool MergedSortedUnprocessed { get; set; }
        public bool MergedSortedProcesed { get; set; }
        public bool SortedGoldenSampleWS { get; set; }
        public bool SortedTestSampleWS { get; set; }

        public Analysis(int recTimeBase, bool mergedSortedUnprocessed, bool mergedSortedProcesed, bool sortedGoldenSampleWS, bool sortedTestSampleWS)
        {
            FilePairs = new List<string>();
            RecTimeBase = recTimeBase;
            MergedSortedUnprocessed = mergedSortedUnprocessed;
            MergedSortedProcesed = mergedSortedProcesed;
            SortedGoldenSampleWS = sortedGoldenSampleWS;
            SortedTestSampleWS = sortedTestSampleWS;
        }

        public void RunCalc(IProgress<int> progress)
        {
            try
            {
                for (int k = 0; k < FilePairs.Count; k = k + 2)
                {
                    if (FStop == true) { goto WorkEnd; }

                    string goldenSample_Name = Path.GetFileName(FilePairs[k]);
                    string testSample_Name = Path.GetFileName(FilePairs[k + 1]);

                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    Dictionary<string, string> colNames = new Dictionary<string, string>();

                    DataSet result = StreamReader(FilePairs[k]);
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }
                    DataTable dt = result.Tables[0];
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    result = StreamReader(FilePairs[k + 1]);
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }
                    DataTable dt2 = result.Tables[0];
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    colNames = SetDictionary(dt, colNames);


                    System.Data.DataColumn newColumn = new System.Data.DataColumn(colNames["DevType"], typeof(System.String));
                    newColumn.DefaultValue = "GoldenSample";
                    dt.Columns.Add(newColumn);
                    dt.AcceptChanges();

                    List<string> GoldenSample_MissingColumns = new List<string>();
                    GoldenSample_MissingColumns = ColumnStatus(dt, GoldenSample_MissingColumns, colNames);
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    System.Data.DataColumn newColumn2 = new System.Data.DataColumn(colNames["DevType"], typeof(System.String));
                    newColumn2.DefaultValue = "TestSample";
                    dt2.Columns.Add(newColumn2);
                    dt2.AcceptChanges();

                    List<string> TestSample_MissingColumns = new List<string>();
                    TestSample_MissingColumns = ColumnStatus(dt2, TestSample_MissingColumns, colNames);
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    if (GoldenSample_MissingColumns.Count > 0 || TestSample_MissingColumns.Count > 0)
                    {
                        string Message = @"";

                        Message = goldenSample_Name + " does not have columns named:" + Environment.NewLine;
                        foreach (string line in GoldenSample_MissingColumns)
                        {
                            Message = Message + line + ";" + Environment.NewLine;
                        }

                        Message = Message + Environment.NewLine;
                        Message = Message + testSample_Name + " does not have columns named:" + Environment.NewLine;

                        foreach (string line in TestSample_MissingColumns)
                        {
                            Message = Message + line + ";" + Environment.NewLine;
                        }

                        Message = Message + Environment.NewLine;
                        Message = Message + "Do you want to continue anyway?";

                        if (MessageBox.Show(Message, "Data columns are missig!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            //Try to continue, see waht happens.
                        }
                        else
                        {
                            dt.Clear();
                            dt2.Clear();
                            goto WorkEnd;
                        }
                    }

                    if (FStop == true) { goto WorkEnd; }
                    if (colNames.ContainsKey("UnixTS") == false)
                    {
                        colNames.Add("UnixTS", "Unix timestamp");

                        dt = DateToUnix(dt, colNames);
                        dt2 = DateToUnix(dt2, colNames);
                    }

                    dt = EPE_Columns(dt, colNames);
                    dt = DublicateRemove(dt);
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    dt2 = EPE_Columns(dt2, colNames);
                    dt2 = DublicateRemove(dt2);
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    colNames.Add("EPE_x", "EPE_x");
                    colNames.Add("EPE_y", "EPE_y");
                    colNames.Add("EPE_XY", "EPE_XY");

                    dt.Merge(dt2, true, MissingSchemaAction.Ignore);
                    dt.DefaultView.Sort = colNames["UnixTS"];
                    dt = dt.DefaultView.ToTable();
                    dt2.Clear();

                    DataTable dtMergedSortedUnprocessed = new DataTable();
                    dtMergedSortedUnprocessed = dt.Copy();

                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                    if (FStop == true) { goto WorkEnd; }

                    int tsColIndex = dt.Columns.IndexOf(colNames["UnixTS"]);
                    int typeColIndex = dt.Columns.IndexOf(colNames["DevType"]);
                    int satColIndex = dt.Columns.IndexOf(colNames["Sat"]);

                    int localPeriodStart = 0;
                    int p = 0;

                    Reset:
                    bool goldenSample = false;
                    bool testSample = false;

                    for (int i = localPeriodStart; i <= dt.Rows.Count - 1; i++)
                    {
                        if (FStop == true) { goto WorkEnd; }

                        if (((15 * i / dt.Rows.Count) - p) >= 1)
                        {
                            p++;
                            ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                        }

                        for (int j = i; j <= dt.Rows.Count - 1; j++)
                        {
                            if (FStop == true) { goto WorkEnd; }

                            long ts_Diff = long.Parse(dt.Rows[j][tsColIndex].ToString()) - long.Parse(dt.Rows[i][tsColIndex].ToString());

                            string ts_i = dt.Rows[i][tsColIndex].ToString();
                            string ts_j = dt.Rows[j][tsColIndex].ToString();

                            if (ts_Diff <= RecTimeBase)
                            {
                                if (dt.Rows[j][typeColIndex].ToString() == "GoldenSample")
                                {
                                    goldenSample = true;
                                }
                                else if (dt.Rows[j][typeColIndex].ToString() == "TestSample")
                                {
                                    testSample = true;
                                }


                                if (j == dt.Rows.Count - 1 && (goldenSample == false || testSample == false))  // Delete new period, if it is within time base but contains only one DevType;
                                {
                                    DataRangeDelete(dt, i, j);
                                    localPeriodStart = i;
                                    goto Reset;
                                }
                            }
                            else if (ts_Diff > RecTimeBase && j != dt.Rows.Count - 1 && j != 0)
                            {
                                if (goldenSample == false || testSample == false)
                                {
                                    DataRangeDelete(dt, i, j - 1);

                                    localPeriodStart = i;
                                    goto Reset;
                                }
                                else
                                {
                                    localPeriodStart = j;
                                    goto Reset;
                                }
                            }
                            else if (ts_Diff > RecTimeBase && j == dt.Rows.Count - 1) // End of data. Delete last point
                            {
                                DataRangeDelete(dt, i, j);
                                localPeriodStart = j;
                                goto Reset;
                            }
                        }
                        goldenSample = false;
                        testSample = false;
                    }




                    typeColIndex = dt.Columns.IndexOf(colNames["DevType"]);
                    DataTable dtGoldenSample = dt.Clone();
                    DataTable dtTestSample = dt.Clone();

                    foreach (DataRow drtableOld in dt.Rows)
                    {
                        if (FStop == true) { goto WorkEnd; }

                        if (drtableOld[typeColIndex].ToString() == "GoldenSample")
                        {
                            dtGoldenSample.ImportRow(drtableOld);
                        }

                        if (drtableOld[typeColIndex].ToString() == "TestSample")
                        {
                            dtTestSample.ImportRow(drtableOld);
                        }
                    }
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));


                    foreach (var column in dtGoldenSample.Columns.Cast<DataColumn>().ToArray())
                    {
                        if (FStop == true) { goto WorkEnd; }

                        if (dtGoldenSample.AsEnumerable().All(dr => dr.IsNull(column)))
                        {
                            if (!colNames.Values.Contains(column.ColumnName))
                            {
                                dtGoldenSample.Columns.Remove(column);
                            }
                        }
                    }
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));

                    foreach (var column in dtTestSample.Columns.Cast<DataColumn>().ToArray())
                    {
                        if (FStop == true) { goto WorkEnd; }

                        if (dtTestSample.AsEnumerable().All(dr => dr.IsNull(column)))
                        {
                            if (!colNames.Values.Contains(column.ColumnName))
                            {
                                dtTestSample.Columns.Remove(column);
                            }
                        }
                    }



                    dtGoldenSample = PeriodEndFix(dtGoldenSample, RecTimeBase, colNames, progress);
                    dtTestSample = PeriodEndFix(dtTestSample, RecTimeBase, colNames, progress);
                    //IFT_Params[0]  AVG Sat count
                    //IFT_Params[1]  Sat loss count
                    //IFT_Params[2]  Time spent without GPS fix
                    //IFT_Params[3]  AVG HDOP
                    //IFT_Params[4]  EPE_XY
                    //IFT_Params[5]  AVG GSM Signal strength
                    //IFT_Params[6]  Operator loss count

                    //AVG Sat count; Sat loss count; Time spent without GPS fix; AVG HDOP; 
                    if (FStop == true) { goto WorkEnd; }
                    decimal[] GoldenSample_IFT_Params = new decimal[7];
                    GoldenSample_IFT_Params = SatLos_SatAVG(dtGoldenSample, colNames, GoldenSample_IFT_Params, progress);
                    decimal[] TestSample_IFT_Params = new decimal[7];
                    TestSample_IFT_Params = SatLos_SatAVG(dtTestSample, colNames, TestSample_IFT_Params, progress);


                    //EPE
                    if (FStop == true) { goto WorkEnd; }
                    GoldenSample_IFT_Params = EPE(dtGoldenSample, colNames, GoldenSample_IFT_Params, progress);
                    TestSample_IFT_Params = EPE(dtTestSample, colNames, TestSample_IFT_Params, progress);


                    ////AVG GSM Signal strength; Operator loss count
                    if (FStop == true) { goto WorkEnd; }
                    GoldenSample_IFT_Params = GSM_P(dtGoldenSample, colNames, GoldenSample_IFT_Params, progress);
                    TestSample_IFT_Params = GSM_P(dtTestSample, colNames, TestSample_IFT_Params, progress);

                    if (FStop == true) { goto WorkEnd; }
                    DataTable dtCompare = new DataTable();
                    dtCompare = MakeComparetTable(GoldenSample_IFT_Params, TestSample_IFT_Params, goldenSample_Name, testSample_Name);

                    string wbName = Path.GetDirectoryName(FilePairs[k]) + "/Results" + ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString() + ".xlsx";
                    XLWorkbook wb = new XLWorkbook();
                    wb = new XLWorkbook(XLEventTracking.Disabled);
                    wb.Worksheets.Add("Comparison");
                    //wb.SaveAs(wbName);
                    //wb.Dispose();
                    GC.Collect();
                    
                    if (FStop == true) { goto WorkEnd; }
                    if (MergedSortedUnprocessed == true)
                    {
                        //wb = new XLWorkbook(wbName);
                        wb.Worksheets.Add(dtMergedSortedUnprocessed, "MergedSortedUnprocesed");
                        //wb.SaveAs(wbName);
                        //wb.Dispose();
                        GC.Collect();
                    }
                    else
                    {
                        dtMergedSortedUnprocessed.Clear();
                        dtMergedSortedUnprocessed.Dispose();
                    }


                    if (FStop == true) { goto WorkEnd; }
                    if (MergedSortedProcesed == true)
                    {
                        //wb = new XLWorkbook(wbName);
                        wb.Worksheets.Add(dt, "MergedSortedProcesed");
                        //wb.SaveAs(wbName);
                        //wb.Dispose();
                        GC.Collect();
                    }
                    else
                    {
                        dt.Clear();
                        dt.Dispose();
                    }
                    ProgressNr = ProgressNr + (decimal)(5.0 / (FilePairs.Count / 2.0));
                    
                    if (FStop == true) { goto WorkEnd; }
                    if (SortedGoldenSampleWS == true)
                    {
                        //wb = new XLWorkbook(wbName);
                        wb.Worksheets.Add(dtGoldenSample, "GoldenSample");
                        //wb.SaveAs(wbName);
                        //wb.Dispose();
                        GC.Collect();
                    }
                    else
                    {
                        dtGoldenSample.Clear();
                        dtGoldenSample.Dispose();
                    }
                    ProgressNr = ProgressNr + (decimal)(5.0 / (FilePairs.Count / 2.0));

                    if (FStop == true) { goto WorkEnd; }
                    if (SortedTestSampleWS == true)
                    {
                        //wb = new XLWorkbook(wbName);
                        wb.Worksheets.Add(dtTestSample, "TestSample");
                        //wb.SaveAs(wbName);
                        //wb.Dispose();
                        GC.Collect();
                    }
                    else
                    {
                        dtTestSample.Clear();
                        dtTestSample.Dispose();
                    }
                    ProgressNr = ProgressNr + (decimal)(5.0 / (FilePairs.Count / 2.0));

                    if (FStop == true) { goto WorkEnd; }
                    //wb = new XLWorkbook(wbName);
                    //wb.Worksheets.Add(dtCompare, "Comparison"); .InsertTable(dtCompare);
                    wb.Worksheet("Comparison").Cell("A1").InsertTable(dtCompare);
                    wb.Worksheet("Comparison").Range("A2:H2").Style.Fill.BackgroundColor = XLColor.FromArgb(0xFFFF5A); //GoldenSample FFCA9B
                    wb.Worksheet("Comparison").Range("A3:H3").Style.Fill.BackgroundColor = XLColor.FromArgb(0xB8DBF3); //TestSample B8DBF3
                    wb.SaveAs(wbName);
                    wb.Dispose();
                    GC.Collect();
                    ProgressNr = ProgressNr + (decimal)(5.0 / (FilePairs.Count / 2.0));

                    if (FStop == true) { goto WorkEnd; }
                    //wb.SaveAs(Path.GetDirectoryName(FilePairs[k]) + "/Results" + ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString() + ".xlsx");
                    ProgressNr = ProgressNr + (decimal)(10.0 / (FilePairs.Count / 2.0));

                    dtMergedSortedUnprocessed.Clear();
                    dtMergedSortedUnprocessed.Dispose();
                    dt.Clear();
                    dt.Dispose();
                    dtGoldenSample.Clear();
                    dtGoldenSample.Dispose();
                    dtTestSample.Clear();
                    dtTestSample.Dispose();
                    dtCompare.Clear();
                    dtCompare.Dispose();
                    //wb.Dispose();
                }


                WorkEnd:

                ProgressNr = 100;
                progress.Report((int)ProgressNr);

                System.Action action = () => MainWindow.CurrentForm.calculateBtn.Content = "Calculate";
                Application.Current.Dispatcher.Invoke(action);
                System.Action action2 = () => MainWindow.CurrentForm.progressBar1.Visibility = Visibility.Hidden;
                Application.Current.Dispatcher.Invoke(action2);

                System.Action action3 = () => MainWindow.CurrentForm.AnalysisTabItemsEnDis(true);
                Application.Current.Dispatcher.Invoke(action3);

                FStop = false;
                FilePairs.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.ToString(), "Exception! Check your input data.", MessageBoxButton.OK);
                
                ProgressNr = 100;
                progress.Report((int)ProgressNr);

                System.Action action = () => MainWindow.CurrentForm.calculateBtn.Content = "Calculate";
                Application.Current.Dispatcher.Invoke(action);
                System.Action action2 = () => MainWindow.CurrentForm.progressBar1.Visibility = Visibility.Hidden;
                Application.Current.Dispatcher.Invoke(action2);

                System.Action action3 = () => MainWindow.CurrentForm.AnalysisTabItemsEnDis(true);
                Application.Current.Dispatcher.Invoke(action3);

                FStop = false;
                FilePairs.Clear();
            }
        }



        private DataTable DataRangeDelete(DataTable dt, int rowStartIndex, int rowEndIndex)
        {
            for (int i = rowStartIndex; i <= rowEndIndex; i++)
            {
                DataRow dr = dt.Rows[rowStartIndex];
                dr.Delete();
                dt.AcceptChanges();
            }
            return dt;
        }

        private DataSet StreamReader(string filePath)
        {
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader;

            //1. Reading Excel file
            if (Path.GetExtension(filePath).ToUpper() == ".XLS")
            {
                //1.1 Reading from a binary Excel file ('97-2003 format; *.xls)
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else
            {
                //1.2 Reading from a OpenXml Excel file (2007 format; *.xlsx)
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }

            DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });

            excelReader.Close();
            excelReader.Dispose();
            stream.Close();
            stream.Dispose();

            return result;
        }

        private Dictionary<string, string> SetDictionary(DataTable dt, Dictionary<string, string> colNames)
        {
            var Type = Source.NA;

            if (dt.Columns.IndexOf("Paskutiniojo GPS data ir laikas") != -1)
            {
                Type = Source.TT2_LT;
            }
            else if (dt.Columns.IndexOf("Last Gps Datetime") != -1)
            {
                Type = Source.TT2_EN;
            }
            else if (dt.Columns.IndexOf("Last Gps Datetime") != -1)
            {
                Type = Source.OffTrack_EN;
            }

            if (Type == Source.TT2_LT)
            {
                colNames.Add("RecTime", "Paskutiniojo GPS data ir laikas");
                colNames.Add("Sat", "Palydovų skaičius");
                colNames.Add("HDOP", "HDOP");
                colNames.Add("EPE", "5011");
                colNames.Add("Op", "Operatorius");
                colNames.Add("GSM_SignS", "GSM signalo stiprumas");
                colNames.Add("DevType", "DevType");
            }
            else if (Type == Source.TT2_EN || Type == Source.NA)
            {
                colNames.Add("RecTime", "Last Gps Datetime");
                colNames.Add("Sat", "Satellites Count");
                colNames.Add("HDOP", "HDOP");
                colNames.Add("EPE", "5011");
                colNames.Add("Op", "Operator");
                colNames.Add("GSM_SignS", "GSM Signal Strength");
                colNames.Add("DevType", "DevType");
            }
            else if (Type == Source.OffTrack_EN)
            {
                colNames.Add("RecTime", "RecTimeStampHuman");
                colNames.Add("UnixTS", "RecTimeStamp");
                colNames.Add("Sat", "Satelites");
                colNames.Add("HDOP", "HDOP");
                colNames.Add("EPE", "5011");
                colNames.Add("Op", "Operator");
                colNames.Add("GSM_SignS", "GSM signal strenght");
                colNames.Add("DevType", "DevType");
            }
            return colNames;
        }


        private DataTable PeriodEndFix(DataTable dt, int timeBase, Dictionary<string, string> colNames, IProgress<int> progress)
        {
            int tsColIndex = dt.Columns.IndexOf(colNames["UnixTS"]);
            int satColIndex = dt.Columns.IndexOf(colNames["Sat"]);

            List<string> periods = new List<string>();
            periods.Add(" ---Period 1 ---");
            periods.Add(dt.Rows[0][tsColIndex].ToString());

            int z = 1;
            int p = 0;

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (FStop == true) { break; }
                if ("GoldenSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && (5 * i / dt.Rows.Count) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }
                else if ("TestSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && (5 * i / dt.Rows.Count) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }

                if (i < dt.Rows.Count - 1)
                {
                    long TS_Diff = long.Parse(dt.Rows[i + 1][tsColIndex].ToString()) - long.Parse(dt.Rows[i][tsColIndex].ToString());

                    if (TS_Diff > timeBase)
                    {
                        periods.Add(dt.Rows[i][tsColIndex].ToString());
                        periods.Add(" ---Period " + z++ + " ---");
                        periods.Add(dt.Rows[i + 1][tsColIndex].ToString());

                        if (dt.Rows[i][satColIndex].ToString() != "0") //Nullify period end.
                        {
                            DataRow dr = dt.NewRow();
                            dr.ItemArray = dt.Rows[i].ItemArray.Clone() as object[];
                            dt.Rows.InsertAt(dr, i);
                            dt.AcceptChanges();
                            dt.Rows[i + 1][satColIndex] = "-1"; //Spec. Value indicates period end.
                            dt.AcceptChanges();
                            i++;
                        }
                    }
                }
                else // Spec. Value for the last point in datatable, indicates period end.
                {
                    DataRow dr = dt.NewRow();
                    dr.ItemArray = dt.Rows[i].ItemArray.Clone() as object[];
                    dt.Rows.InsertAt(dr, i);
                    dt.AcceptChanges();
                    dt.Rows[i + 1][satColIndex] = "-1"; //Spec. Value indicates period end.
                    dt.AcceptChanges();
                    i++;
                    //break;
                }

            }
            return dt;
        }

        private List<string> ColumnStatus(DataTable dt, List<string> missingColumns, Dictionary<string, string> colNames)
        {
            string gsm_UMTS_SS = "GSM/UMTS signal strength";
            string gsm_UMTS_OP = "GSM/UMTS operator";

            if (colNames["RecTime"] == "Paskutiniojo GPS data ir laikas")
            {
                gsm_UMTS_SS = "GSM/UMTS signalo stiprumas";
                gsm_UMTS_OP = "GSM/UMTS operatorius";
            }



            foreach (KeyValuePair<string, string> entry in colNames)
            {
                if (FStop == true) { break; }

                if (dt.Columns.IndexOf(entry.Value) == -1 && entry.Value != colNames["GSM_SignS"] && entry.Value != colNames["Op"])
                {
                    missingColumns.Add(entry.Value);
                }
                else if (dt.Columns.IndexOf(entry.Value) == -1 && entry.Value == colNames["GSM_SignS"] && dt.Columns.IndexOf(gsm_UMTS_SS) == -1)
                {
                    missingColumns.Add(entry.Value);
                }
                else if (dt.Columns.IndexOf(entry.Value) == -1 && entry.Value == colNames["Op"] && dt.Columns.IndexOf(gsm_UMTS_OP) == -1)
                {
                    missingColumns.Add(entry.Value);
                }
                else if (dt.Columns.IndexOf(entry.Value) == -1 && entry.Value == colNames["GSM_SignS"] && dt.Columns.IndexOf(gsm_UMTS_SS) != -1)
                {
                    dt.Columns[dt.Columns.IndexOf(gsm_UMTS_SS)].ColumnName = colNames["GSM_SignS"];
                    dt.AcceptChanges();
                }
                else if (dt.Columns.IndexOf(entry.Value) == -1 && entry.Value == colNames["Op"] && dt.Columns.IndexOf(gsm_UMTS_OP) != -1)
                {
                    dt.Columns[dt.Columns.IndexOf(gsm_UMTS_OP)].ColumnName = colNames["Op"];
                    dt.AcceptChanges();
                }
            }
            return missingColumns;
        }

        private DataTable DateToUnix(DataTable dt, Dictionary<string, string> colNames)
        {
            if (dt.Columns.IndexOf(colNames["UnixTS"]) < 0)
            {
                dt.Columns.Add(colNames["UnixTS"], typeof(Int32)).SetOrdinal(dt.Columns.IndexOf(colNames["RecTime"]) + 1);
                dt.AcceptChanges();

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    DateTime dateTime = DateTime.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["RecTime"])].ToString());
                    Int32 unixTimestamp = (Int32)(dateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    dt.Rows[i][dt.Columns.IndexOf(colNames["UnixTS"])] = unixTimestamp;
                    dt.AcceptChanges();
                }
            }
            else
            {
                //Use existing UnixTS
            }

            return dt;
        }

        private DataTable EPE_Columns(DataTable dt, Dictionary<string, string> colNames)
        {
            if (dt.Columns.IndexOf(colNames["EPE"]) != -1)
            {
                dt.Columns.Add("EPE_x", typeof(double)).SetOrdinal(dt.Columns.IndexOf(colNames["EPE"]) + 1);
                dt.AcceptChanges();

                dt.Columns.Add("EPE_y", typeof(double)).SetOrdinal(dt.Columns.IndexOf(colNames["EPE"]) + 2);
                dt.AcceptChanges();

                dt.Columns.Add("EPE_XY", typeof(double)).SetOrdinal(dt.Columns.IndexOf(colNames["EPE"]) + 3);
                dt.AcceptChanges();
            }

            return dt;
        }

        private DataTable DublicateRemove(DataTable dt)
        {
            string[] columnNames = (from dc in dt.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();

            for (int i = 0; i < columnNames.Count(); i++)
            {
                if (FStop == true) { break; }
                for (int j = 0; j < columnNames.Count(); j++)
                {
                    if (columnNames[i].Contains(columnNames[j] + "_1"))
                    {
                        dt.Columns.Remove(columnNames[j] + "_1");
                        dt.AcceptChanges();
                    }
                }
            }
            return dt;
        }

        private decimal[] SatLos_SatAVG(DataTable dt, Dictionary<string, string> colNames, decimal[] ift_Params, IProgress<int> progress)
        {
            decimal satCount = 0;
            decimal hdopCount = 0;
            decimal notZero = 0;

            bool periodStart = false;
            bool periodEnd = false;

            long ts_Start = 0;
            long ts_End = 0;
            decimal timeNoFix = 0;
            decimal satLosCount = 0;

            int p = 0;

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (FStop == true) { break; }

                if ("GoldenSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && ((5 * i / dt.Rows.Count)) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }
                else if ("TestSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && ((5 * i / dt.Rows.Count)) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }

                if (long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) > 0 && long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) < 40)
                {
                    satCount += decimal.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString());
                    hdopCount += decimal.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["HDOP"])].ToString());
                    notZero++;
                }

                if (long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) <= 0 && periodStart == false)
                {
                    periodStart = true;
                    ts_Start = long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["UnixTS"])].ToString());

                    if (i != 0)
                    {
                        if (long.Parse(dt.Rows[i - 1][dt.Columns.IndexOf(colNames["Sat"])].ToString()) > 0)
                        {
                            satLosCount++;
                        }
                    }
                }
                else if ((long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) > 0 && long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) < 40 && periodStart == true)
                    || (i == dt.Rows.Count - 1 && periodStart == true))
                {
                    periodEnd = true;
                    ts_End = long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["UnixTS"])].ToString());


                    if (periodStart == true && periodEnd == true)
                    {
                        timeNoFix = timeNoFix + ts_End - ts_Start;

                        periodStart = false;
                        periodEnd = false;
                    }
                }
            }

            ift_Params[0] = decimal.Round(satCount / notZero, 3, MidpointRounding.AwayFromZero);
            ift_Params[1] = satLosCount;
            ift_Params[2] = timeNoFix;
            ift_Params[3] = decimal.Round(hdopCount / notZero, 5, MidpointRounding.AwayFromZero);

            return ift_Params;
        }


        private decimal[] GSM_P(DataTable dt, Dictionary<string, string> colNames, decimal[] ift_Params, IProgress<int> progress)
        {
            decimal signalStrength = 0;
            decimal notZero = 0;

            bool periodStart = false;
            bool periodEnd = false;

            int p = 0;
            //long ts_Start = 0;
            //long ts_End = 0;
            //decimal timeNoOp = 0;
            decimal OpLosCount = 0;

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (FStop == true) { break; }

                if ("GoldenSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && ((5 * i / dt.Rows.Count)) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }
                else if ("TestSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && ((5 * i / dt.Rows.Count)) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }

                if (long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString()) > 0 && long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString()) < 33)
                {
                    signalStrength = signalStrength + decimal.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString());
                    notZero++;
                }

                if (long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString()) == 0 && periodStart == false)
                {
                    periodStart = true;
                    //TS_Start = long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["UnixTS"])].ToString());

                    if (i != 0)
                    {
                        if (long.Parse(dt.Rows[i - 1][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString()) != 0)
                        {
                            OpLosCount++;
                        }
                    }
                }
                else if ((long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString()) != 0 && long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["GSM_SignS"])].ToString()) < 40 && periodStart == true)
                    || (i == dt.Rows.Count - 1 && periodStart == true))
                {
                    periodEnd = true;
                    //TS_End = long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["UnixTS"])].ToString());


                    if (periodStart == true && periodEnd == true)
                    {
                        //TimeNoOp = TimeNoOp + TS_End - TS_Start;

                        periodStart = false;
                        periodEnd = false;
                    }
                }
            }

            ift_Params[5] = decimal.Round(signalStrength / notZero, 3, MidpointRounding.AwayFromZero);
            ift_Params[6] = OpLosCount;

            return ift_Params;
        }


        private decimal[] EPE(DataTable dt, Dictionary<string, string> colNames, decimal[] ift_Params, IProgress<int> progress)
        {
            double epe = 0;
            double epe_x = 0;
            double epe_y = 0;
            double epe_XY = 0;
            double notFFs = 0;

            double sum_EPE_XY = 0;

            int p = 0;

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (FStop == true) { break; }

                if ("GoldenSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && ((10 * i / dt.Rows.Count)) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }
                else if ("TestSample" == dt.Rows[i][dt.Columns.IndexOf(colNames["DevType"])].ToString() && ((10 * i / dt.Rows.Count)) - p >= 1)
                {
                    p++;
                    ProgressNr += (decimal)(1.0 / (FilePairs.Count / 2.0));
                }

                if (long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) > 0 && long.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["Sat"])].ToString()) < 40)
                {
                    epe = double.Parse(dt.Rows[i][dt.Columns.IndexOf(colNames["EPE"])].ToString());

                    epe_x = Math.Truncate(epe / Math.Pow(2, 32));
                    dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_x"])] = epe_x;
                    epe_y = (epe % Math.Pow(2, 32));
                    dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_y"])] = epe_y;

                    if (epe_x < 4294964796 && epe_y < 4294964796) //FFFF63C || FFFF63C
                    {
                        epe_XY = Math.Sqrt((epe_x * epe_x) + (epe_y * epe_y));
                        dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_XY"])] = epe_XY;

                        sum_EPE_XY += epe_XY;
                        notFFs++;
                    }
                    else if (epe_x < 4294967295 && epe_y > 4294967295)
                    {
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_x"])] = EPE_x;
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_y"])] = -1;
                    }
                    else if (epe_x > 4294967295 && epe_y < 4294967295)
                    {
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_x"])] = -1;
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_y"])] = EPE_y;
                    }
                    else
                    {
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_XY"])] = -1;
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_x"])] = -1;
                        //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_y"])] = -1;
                    }
                }
                else
                {
                    //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_XY"])] = -1;
                    //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_x"])] = -1;
                    //dt.Rows[i][dt.Columns.IndexOf(colNames["EPE_y"])] = -1;
                }
            }

            ift_Params[4] = decimal.Round((decimal)(sum_EPE_XY / notFFs), 6, MidpointRounding.AwayFromZero);
            return ift_Params;
        }



        private DataTable MakeComparetTable(decimal[] goldenSample_IFT_Params, decimal[] testSample_IFT_Params, string goldenSample_Name, string testSample_Name)
        {
            // Create a new DataTable.
            DataTable compare = new DataTable("CompareTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn column;
            DataRow row;

            // Create new DataColumn, set DataType,
            // ColumnName and add to DataTable.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Name";
            column.ReadOnly = false;
            column.Unique = true;
            // Add the Column to the DataColumnCollection.
            compare.Columns.Add(column);

            // Create 2nd column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Average satelite count";
            column.AutoIncrement = false;
            column.Caption = "Average satelite count";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Create 3rd column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "GNSS fix loss count";
            column.AutoIncrement = false;
            column.Caption = "GNSS fix loss count";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Create 4th column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Time spent without GNSS fix";
            column.AutoIncrement = false;
            column.Caption = "Time spent without GNSS fix";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Create 5th column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Average HDOP";
            column.AutoIncrement = false;
            column.Caption = "Average HDOP";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Create 6th column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Average EPE value";
            column.AutoIncrement = false;
            column.Caption = "Average EPE value";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Create 7th column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Average GSM signal strength";
            column.AutoIncrement = false;
            column.Caption = "Average GSM signal strength";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Create 8th column.
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Operator loss count";
            column.AutoIncrement = false;
            column.Caption = "Operator loss count";
            column.ReadOnly = false;
            column.Unique = false;
            // Add the column to the table.
            compare.Columns.Add(column);

            // Make the ID column the primary key column.
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = compare.Columns["Name"];
            compare.PrimaryKey = PrimaryKeyColumns;

            // Instantiate the DataSet variable.
            DataSet dataSet = new DataSet();
            // Add the new DataTable to the DataSet.
            dataSet.Tables.Add(compare);

            // Create three new DataRow objects and add
            // them to the DataTable
            for (int i = 0; i <= 1; i++)
            {
                row = compare.NewRow();
                if (i == 0)
                {
                    row["Name"] = goldenSample_Name;
                    row["Average satelite count"] = goldenSample_IFT_Params[0];
                    row["GNSS fix loss count"] = goldenSample_IFT_Params[1];
                    row["Time spent without GNSS fix"] = goldenSample_IFT_Params[2];
                    row["Average HDOP"] = goldenSample_IFT_Params[3];
                    row["Average EPE value"] = goldenSample_IFT_Params[4];
                    row["Average GSM signal strength"] = goldenSample_IFT_Params[5];
                    row["Operator loss count"] = goldenSample_IFT_Params[6];
                    compare.Rows.Add(row);
                }
                if (i == 1)
                {
                    row["Name"] = testSample_Name;
                    row["Average satelite count"] = testSample_IFT_Params[0];
                    row["GNSS fix loss count"] = testSample_IFT_Params[1];
                    row["Time spent without GNSS fix"] = testSample_IFT_Params[2];
                    row["Average HDOP"] = testSample_IFT_Params[3];
                    row["Average EPE value"] = testSample_IFT_Params[4];
                    row["Average GSM signal strength"] = testSample_IFT_Params[5];
                    row["Operator loss count"] = testSample_IFT_Params[6];
                    compare.Rows.Add(row);
                }
            }
            return compare;
        }
    }
}
