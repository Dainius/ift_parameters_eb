﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FT_Parameters
{
    public static class  Credentials
    {
        public static string Username { get; set; }
        public static string Password { get; set; }
    }
}
