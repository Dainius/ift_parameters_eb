﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FT_Parameters
{
    
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            SetCredentials();
            MainWindow.CurrentForm.downloadBtn.IsEnabled = false;
        }

        private void SetCredentials()
        {
            if (Credentials.Username != null && Credentials.Username.Length > 0)
            {
                userNamePswBox.Password = Credentials.Username;
            }

            if (Credentials.Password != null && Credentials.Password.Length > 0)
            {
                passwordPswBox.Password = Credentials.Password;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.CurrentForm.downloadBtnEnDis();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (userNamePswBox.Password != null)
            {
                Credentials.Username = userNamePswBox.Password;
            }

            if (passwordPswBox.Password != null)
            {
                Credentials.Password = passwordPswBox.Password;
            }
            Close();
        }
    }
}
