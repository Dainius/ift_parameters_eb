﻿namespace FT_Parameters
{
    public enum Source
    {
        TT2_LT,
        TT2_EN,
        OffTrack_EN,
        NA,
    }
}
