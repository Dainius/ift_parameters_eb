﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;

namespace FT_Parameters
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>


    public partial class MainWindow : Window
    {
        public List<Files> FilesList { get; set; }
        public string Path { get; set; }
        public string CustomPath { get; set; }
        public int TimeOffset { get; set; }

        public static MainWindow CurrentForm;

        public static bool StopDownloading = false;
        public static bool DownloadingStopped = false;
        public static bool UserClick = false;

        public static Settings SettingsWindow;

        public MainWindow()
        {
            CurrentForm = this;
            #region Download tab
            InitializeComponent();

            TimeOffset = -3;
            timeOffsetUpDownControl.Value = TimeOffset;

            CreateFolders("/DownloadedFiles");
            dtpStartTime.Value = DateTime.Today;
            dtpEndTime.Value = DateTime.Today.AddSeconds(86399);

            FilesList = new List<Files>();
            downloadsDataGrid.ItemsSource = FilesList;

            //FilesList.Add(new Files { DeviceName = "fmp5", DeviceType = "Test", ObjectID = "8310383", PeriodStart = "2020-09-01 00:00:00", PeriodEnd = "2020-09-19 23:59:59", Status = "" });
            //FilesList.Add(new Files { DeviceName = "fmp4", DeviceType = "Golden", ObjectID = "8310384", PeriodStart = "2020-09-01 00:00:00", PeriodEnd = "2020-09-19 23:59:59", Status = "" });

            deleteBtn.IsEnabled = false;
            addBtn.IsEnabled = false;
            downloadBtnEnDis();
            #endregion

            #region Analysis tab
            recTimeBaseUpDownControl.Value = 10;
            this.progressBar1.Visibility = Visibility.Hidden;
            this.selectParentFolderGrpBox.IsEnabled = false;
            calculateBtn.IsEnabled = false;
            #endregion
        }

        #region Download tab
        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            if (FilesList.Any(n => n.DeviceName == fileNameTxtBox.Text && n.DeviceType == deviceTypeCmbBox.Text && n.PeriodStart == ((DateTime)dtpStartTime.Value).AddHours(TimeOffset).ToString("yyyy-MM-dd HH:mm:ss") && n.PeriodEnd == ((DateTime)dtpEndTime.Value).AddHours(TimeOffset).ToString("yyyy-MM-dd HH:mm:ss")))
            {
                MessageBox.Show($"Table already contains device {fileNameTxtBox.Text} with type {deviceTypeCmbBox.Text} and data periods from {((DateTime)dtpStartTime.Value).AddHours(TimeOffset).ToString("yyyy-MM-dd HH:mm:ss")} to {((DateTime)dtpEndTime.Value).AddHours(TimeOffset).ToString("yyyy-MM-dd HH:mm:ss")}.");
            }
            else
            {
                Files dw = new Files() { DeviceName = fileNameTxtBox.Text, DeviceType = deviceTypeCmbBox.Text, ObjectID = objIDTxtBox.Text, PeriodStart = (((DateTime)dtpStartTime.Value).AddHours(TimeOffset)).ToString("yyyy-MM-dd HH:mm:ss"), PeriodEnd = ((DateTime)dtpEndTime.Value).AddHours(TimeOffset).ToString("yyyy-MM-dd HH:mm:ss") };
                FilesList.Add(dw);
                downloadsDataGrid.Items.Refresh();
                DtGridViewRefresh();
                deleteBtn.IsEnabled = false;
            }          
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            List<Files> selectedRowsList = new List<Files>();
            for (int i = 0; i < downloadsDataGrid.SelectedItems.Count; i++)
            {
                selectedRowsList.Add((Files)downloadsDataGrid.SelectedItems[i]);
            }

            var selectedRow = downloadsDataGrid.ItemsSource as List<Files>;

            foreach (var item in selectedRowsList)
            {
                selectedRow.Remove(item);
            }
            DtGridViewRefresh();
            deleteBtn.IsEnabled = false;
        }

        public void DtGridViewRefresh()
        {
            downloadsDataGrid.Items.Refresh();
            downloadsDataGrid.ItemsSource = null;
            downloadsDataGrid.ItemsSource = FilesList;

            downloadBtnEnDis();
        }

        private void downloadsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FilesList.Count() > 0 && downloadsDataGrid.IsEnabled == true)
            {
                deleteBtn.IsEnabled = true;
            }
        }

        private void downloadBtn_Click(object sender, RoutedEventArgs e)
        {
            fileNameTxtBox.IsEnabled = false;
            objIDTxtBox.IsEnabled = false;
            dtpStartTime.IsEnabled = false;
            dtpEndTime.IsEnabled = false;
            timeOffsetUpDownControl.IsEnabled = false;
            deleteBtn.IsEnabled = false;
            addBtn.IsEnabled = false;
            saveBtn.IsEnabled = false;
            loadBtn.IsEnabled = false;
            downloadsDataGrid.IsEnabled = false;
            deviceTypeCmbBox.IsEnabled = false;
            downloadOptionGrpBox.IsEnabled = false;
            downloadBtn.IsEnabled = false;

            UserClick = true;
            DownloadBtnRenaming();
            UserClick = false;

            if (StopDownloading == false && (downloadBtn.Content.ToString() == "Downloading" || downloadBtn.Content.ToString() == "Stop downloading"))
            {
                Download();
            }

        }

        private async void Download()
        {
            foreach (var file in FilesList)
            {
                file.Status = "";
            }


            ServerConnection sc = new ServerConnection();
            ServerConnection.DownloadComplete.Clear();

            foreach (var item in FilesList)
            {
                ServerConnection.DownloadComplete.Add(false);
            }

            await Task.Run(() => sc.Authorise());

            int foldersCount = 0;
            if (FilesList.Count % 2 == 0)
            {
                foldersCount = FilesList.Count / 2;
            }
            else
            {
                foldersCount = FilesList.Count / 2 + 1;
            }

            try
            {
                if (pairDownloadsChkBox.IsChecked == true)
                {
                    List<string> pathsList = new List<string>();
                    for (int i = 0; i < foldersCount; i++)
                    {
                        pathsList.Add(CreateFolders($"\\{((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds)}_Pair_{i}"));
                    }

                    foreach (var file in FilesList)
                    {
                        if (StopDownloading == false)
                        {
                            System.Action action2 = () => sc.Download($"{file.DeviceName}_{file.DeviceType}_{FilesList.IndexOf(file)}_{((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds)}", 
                                $"{file.ObjectID}/",
                                $"{file.PeriodStart.Replace(" ", "T")}.000Z/",
                                $"{file.PeriodEnd.Replace(" ", "T")}.000Z",
                                pathsList[(FilesList.IndexOf(file) / 2)],
                                (FilesList.IndexOf(file)));
                            AsyncRunner(action2);

                            if (downloadSimultaneouslyChkBox.IsChecked == true)
                            {
                                System.Action action3 = () => sc.LocalStatusUpdate($"{file.DeviceName}_{file.DeviceType}_{FilesList.IndexOf(file)}", pathsList[(FilesList.IndexOf(file) / 2)], file, FilesList.IndexOf(file));
                                AsyncRunner(action3);
                            }
                            else
                            {
                                await Task.Run(() => sc.LocalStatusUpdate($"{file.DeviceName}_{file.DeviceType}_{FilesList.IndexOf(file)}", pathsList[(FilesList.IndexOf(file) / 2)], file, FilesList.IndexOf(file)));
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    CreateFolders($"\\{((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds)}_FullBatch");
                    foreach (var file in FilesList)
                    {
                        if (StopDownloading == false)
                        {
                            System.Action action2 = () => sc.Download($"{file.DeviceName}_{file.DeviceType}_{FilesList.IndexOf(file)}_{((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds)}", 
                                $"{file.ObjectID}/", 
                                $"{file.PeriodStart.Replace(" ", "T")}.000Z/", 
                                $"{file.PeriodEnd.Replace(" ", "T")}.000Z", 
                                Path, FilesList.IndexOf(file));
                            AsyncRunner(action2);

                            if (downloadSimultaneouslyChkBox.IsChecked == true)
                            {
                                System.Action action3 = () => sc.LocalStatusUpdate($"{file.DeviceName}_{file.DeviceType}_{FilesList.IndexOf(file)}", Path, file, FilesList.IndexOf(file));
                                AsyncRunner(action3);
                            }
                            else
                            {
                                await Task.Run(() => sc.LocalStatusUpdate($"{file.DeviceName}_{file.DeviceType}_{FilesList.IndexOf(file)}", Path, file, FilesList.IndexOf(file)));
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                System.Action action = () => WaitForCompletion(sc);
                AsyncRunner(action);
            }
        }

        private void WaitForCompletion(ServerConnection sc)
        {
            while (ServerConnection.DownloadComplete.Any(c => c == false) && !DownloadingStopped)
            {
                Thread.Sleep(2000);
            }

            List<Action> actionsList = new List<Action>();
            actionsList.Add(() => fileNameTxtBox.IsEnabled = true);
            actionsList.Add(() => objIDTxtBox.IsEnabled = true);
            actionsList.Add(() => dtpStartTime.IsEnabled = true);
            actionsList.Add(() => dtpEndTime.IsEnabled = true);
            actionsList.Add(() => timeOffsetUpDownControl.IsEnabled = true);
            actionsList.Add(() => addBtnEnDis());
            actionsList.Add(() => saveBtn.IsEnabled = true);
            actionsList.Add(() => loadBtn.IsEnabled = true);
            actionsList.Add(() => downloadsDataGrid.IsEnabled = true);
            actionsList.Add(() => downloadOptionGrpBox.IsEnabled = true);
            actionsList.Add(() => deviceTypeCmbBox.IsEnabled = true);
            actionsList.Add(() => downloadBtnEnDis());
            actionsList.Add(() => deleteBtn.IsEnabled = false);
            actionsList.Add(() => DownloadBtnRenaming());

            foreach (var action in actionsList)
            {
                try
                {
                    Dispatcher.Invoke(action);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Method 'WaitForCompletion': {ex.Message}");
                }  
            }

            StopDownloading = false;
            DownloadingStopped = false;
        }

        private void DownloadBtnRenaming()
        {
            if (downloadSimultaneouslyChkBox.IsChecked == true)
            {
                if (downloadBtn.Content.ToString() == "Downloading")
                {
                    downloadBtn.Content = "Download";
                }
                else
                {
                    downloadBtn.Content = "Downloading";
                }
            }
            else
            {
                if (downloadBtn.Content.ToString() == "Download")
                {
                    //Start of download;
                    downloadBtn.Content = "Stop downloading";
                    StopDownloading = false;
                    DownloadingStopped = false;
                }
                else if (downloadBtn.Content.ToString() == "Stop downloading" && UserClick == true)
                {
                    //Stop downloading;
                    downloadBtn.Content = "Stopping...";
                    StopDownloading = true;
                    UserClick = false;
                }
                else if (downloadBtn.Content.ToString() == "Stop downloading" && UserClick == false)
                {
                    //Stop downloading;
                    downloadBtn.Content = "Download";
                    StopDownloading = true;
                }
                else if (DownloadingStopped == true && downloadBtn.Content.ToString() == "Stopping...")
                {
                    //Stopped;
                    downloadBtn.Content = "Download";
                }
                else
                {
                    downloadBtn.Content = "Download";
                }
            }    
        }


        private async void AsyncRunner(Action action)
        {
            await Task.Run(action);
        }

        private string CreateFolders(string destination)
        {
            string directoryName = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            string localPath = new Uri(directoryName).LocalPath;

            if (CustomPath == null)
            {
                Path = localPath + destination;
            }
            else
            {
                Path = CustomPath + destination;
            }

            try
            {
                if (!Directory.Exists(Path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Path);
                    
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Creating new directory failed: " + e.ToString(), "Exception");
            }
            return Path;
        }

        public void downloadBtnEnDis()
        {
            if (FilesList.Count > 0 && downloadsDataGrid.IsEnabled == true)
            {
                if (Credentials.Username != null && Credentials.Username.Length > 0 && Credentials.Password != null && Credentials.Password.Length > 0)
                {

                    downloadBtn.IsEnabled = true;
                    downloadOptionGrpBox.IsEnabled = true;

                    if (downloadBtn.Content.ToString() == "Stop downloading")
                    {
                        if (downloadSimultaneouslyChkBox.IsChecked == false)
                        {
                            downloadBtn.IsEnabled = true;
                        }
                        else
                        {
                            downloadBtn.IsEnabled = false;
                        }
                    }
                }
                else
                {
                    downloadBtn.IsEnabled = false;
                }
                saveBtn.IsEnabled = true;
            }
            else
            {
                saveBtn.IsEnabled = false;
                downloadBtn.IsEnabled = false;

                if (downloadBtn.Content.ToString() == "Stop downloading")
                {
                    if (downloadSimultaneouslyChkBox.IsChecked == false)
                    {
                        downloadBtn.IsEnabled = true;
                    }
                    else
                    {
                        downloadBtn.IsEnabled = false;
                    }
                }
            }
        }

        private void addBtnEnDis()
        {
            if (fileNameTxtBox.Text.Length > 0 && objIDTxtBox.Text.Length > 0 && dtpStartTime.Value.ToString().Length > 0 && dtpEndTime.Value.ToString().Length >0)
            {
                addBtn.IsEnabled = true;
            }
            else
            {
                addBtn.IsEnabled = false;
            }
        }

        private void fileNameTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            addBtnEnDis();
        }

        private void objIDTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            addBtnEnDis();
        }

        private void dtpStartTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            addBtnEnDis();
        }

        private void dtpEndTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            addBtnEnDis();
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            #region saveBin
            //SaveFileDialog saveFileDialog = new SaveFileDialog();
            //saveFileDialog.Filter = "Bin files (*.bin)|*.bin|All files (*.*)|*.*";
            //if (saveFileDialog.ShowDialog() == true)
            //{
            //    SaveLoadList sll = new SaveLoadList();
            //    string filePath = $"{saveFileDialog.FileName}";
            //    sll.WriteToBinaryFile(filePath, FilesList, false);
            //}
            #endregion

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Xml files (*.xml)|*.xml|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                SaveLoadList sll = new SaveLoadList();
                string filePath = $"{saveFileDialog.FileName}";
                sll.WriteToXmlFile(filePath, FilesList, false);
            }
        }

        private void loadBtn_Click(object sender, RoutedEventArgs e)
        {
            #region loadBin
            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.Filter = "Bin files (*.bin)|*.bin|All files (*.*)|*.*";
            //openFileDialog.Multiselect = false;

            //if (openFileDialog.ShowDialog() == true)
            //{
            //    SaveLoadList sll = new SaveLoadList();
            //    FilesList =  sll.ReadFromBinaryFile(openFileDialog.FileName);
            //}
            #endregion

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Xml files (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog.Multiselect = false;

            if (openFileDialog.ShowDialog() == true)
            {
                SaveLoadList sll = new SaveLoadList();
                FilesList = sll.ReadFromXml(openFileDialog.FileName);
            }
            DtGridViewRefresh();
        }


        private void settingsBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SettingsWindow == null)
            {
                SettingsWindow = new Settings();
                SettingsWindow.Closed += (a, b) => SettingsWindow = null;
                SettingsWindow.Show();
            }
            else
            {
                SettingsWindow.Activate();
                SettingsWindow.Show();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (SettingsWindow != null)
            {
                SettingsWindow.Close();
            }
        }

        private void downloadsFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folderBrowserDialog1 = new WinForms.FolderBrowserDialog();
            WinForms.DialogResult result = folderBrowserDialog1.ShowDialog();
            CustomPath = folderBrowserDialog1.SelectedPath;
        }

        private void timeOffsetUpDownControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (timeOffsetUpDownControl.Value <= 23 && timeOffsetUpDownControl.Value >= -23)
            {
                TimeOffset = (int)timeOffsetUpDownControl.Value;
            }
            else
            {
                timeOffsetUpDownControl.Value = TimeOffset;
            }
        }
        #endregion




        #region Analysis tab
        private void calculateBtn_Click(object sender, RoutedEventArgs e)
        {
            Analysis an = new Analysis((int)recTimeBaseUpDownControl.Value, (bool)mergedUnpDSChkBox.IsChecked, (bool)mergedProcSDChkBox.IsChecked, (bool)sortedGldSampleWSChkBox.IsChecked, (bool)sortedTestSampleWSChkBox.IsChecked);

            if ((string)calculateBtn.Content == "Calculate" && Analysis.FStop == false)
            {
                an.ProgressNr = 0;
                progressBar1.Maximum = 100;
                //progressBar1.Step = 1;

                var progress = new Progress<int>(v =>
                {
                    // This lambda is executed in context of UI thread,
                    // so it can safely update form controls
                    progressBar1.Value = v;
                });


                if (filePairRadioBtn.IsChecked == true)
                {
                    an.FilePairs.Clear();

                    an.FilePairs.Add(goldenSampleTxtBox.Text);
                    an.FilePairs.Add(testSampleTxtBox.Text);

                    Action action = () => an.RunCalc(progress);
                    AsyncRunner(action);
                }
                else if (batchOfFilesRadioBtn.IsChecked == true)
                {
                    an.FilePairs.Clear();

                    an.FileBatchFolder_Name = batchOfFilesTxtBox.Text;

                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(an.FileBatchFolder_Name);
                    WalkDirectoryTree(di, an);

                    Action action = () => an.RunCalc(progress);
                    AsyncRunner(action);
                }

                Action action2 = () => UpdateTick(progress, an);
                AsyncRunner(action2);

                calculateBtn.Content = "Stop";
                progressBar1.Visibility = Visibility.Visible;

                AnalysisTabItemsEnDis(false);

            }
            else if ((string)calculateBtn.Content == "Stop" && Analysis.FStop == false)
            {
                calculateBtn.Content = "Stopping...";
                Analysis.FStop = true;
            }
        }

        public void AnalysisTabItemsEnDis(bool isEnabled)
        {
            if (filePairRadioBtn.IsChecked == true)
            {
                selectTwoExcelFilesGrpBox.IsEnabled = isEnabled;
            }
            else if (batchOfFilesRadioBtn.IsChecked == true)
            {
                selectParentFolderGrpBox.IsEnabled = isEnabled;
            }
            filePairRadioBtn.IsEnabled = isEnabled;
            batchOfFilesRadioBtn.IsEnabled = isEnabled;
            saveToWbGrpBox.IsEnabled = isEnabled;
            recTimeBaseUpDownControl.IsEnabled = isEnabled;
        }

        private void UpdateTick(IProgress<int> progress, Analysis an)
        {
            while (an.ProgressNr < 100)
            {
                progress.Report((int)an.ProgressNr);
                Thread.Sleep(100);
            }
        }

        private void WalkDirectoryTree(System.IO.DirectoryInfo root, Analysis an)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            // First, process all the files directly under this folder
            try
            {
                files = root.GetFiles("*.*");
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show(e.Message);
                // This code just writes out the message and continues to recurse.
                // You may decide to do something different here. For example, you
                // can try to elevate your privileges and access the file again.
            }

            catch (System.IO.DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            string goldenSample = "NA";
            string testSample = "NA";

            if (files != null)
            {
                foreach (System.IO.FileInfo fi in files)
                {
                    if (Analysis.FStop == true) { break; }
                    // In this example, we only access the existing FileInfo object. If we
                    // want to open, delete or modify the file, then
                    // a try-catch block is required here to handle the case
                    // where the file has been deleted since the call to TraverseTree().

                    if (fi.Name.Contains("Golden"))
                    {
                        goldenSample = fi.FullName;
                    }
                    else if (fi.Name.Contains("Test"))
                    {
                        testSample = fi.FullName;
                    }
                }

                if (goldenSample != "NA" && testSample != "NA")
                {
                    an.FilePairs.Add(goldenSample);
                    an.FilePairs.Add(testSample);
                }

                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    if (Analysis.FStop == true) { break; }
                    // Resursive call for each subdirectory.
                    WalkDirectoryTree(dirInfo, an);
                }
            }
        }

        private void goldenSampleBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = OpenFileDlg();

            if (openFileDialog1.ShowDialog() == true)
            {
                goldenSampleTxtBox.Text = openFileDialog1.FileName;

            }
        }

        private void testSampleBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = OpenFileDlg();

            if (openFileDialog1.ShowDialog() == true)
            {
                testSampleTxtBox.Text = openFileDialog1.FileName;

            }
        }

        private OpenFileDialog OpenFileDlg()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Excel Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "xlsx",
                Filter = "Excel files (*.xlsx;*.xlx)|*.xlsx;*.xlx",
                FilterIndex = 2,
                RestoreDirectory = false,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            return openFileDialog1;
        }

        private void filePairRadioBtn_Checked(object sender, RoutedEventArgs e)
        {
            if (filePairRadioBtn.IsChecked == true)
            {
                selectTwoExcelFilesGrpBox.IsEnabled = true;
                if (selectParentFolderGrpBox != null)
                {
                    selectParentFolderGrpBox.IsEnabled = false;
                }
                CalcBtnEnDis();
            }
        }

        private void batchOfFilesRadioBtn_Checked(object sender, RoutedEventArgs e)
        {
            if (batchOfFilesRadioBtn.IsChecked == true)
            {
                selectParentFolderGrpBox.IsEnabled = true;
                if (selectTwoExcelFilesGrpBox != null)
                {
                    selectTwoExcelFilesGrpBox.IsEnabled = false;
                }
                CalcBtnEnDis();
            }
        }

        private void batchOfFilesBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folderBrowserDialog1 = new WinForms.FolderBrowserDialog();
            WinForms.DialogResult result = folderBrowserDialog1.ShowDialog();
            batchOfFilesTxtBox.Text = folderBrowserDialog1.SelectedPath;
        }




        private void goldenSampleTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CalcBtnEnDis();
        }

        private void testSampleTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CalcBtnEnDis();
        }

        private void batchOfFilesTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CalcBtnEnDis();
        }

        private void CalcBtnEnDis()
        {
            if (filePairRadioBtn.IsChecked == true && calculateBtn != null)
            {
                if (goldenSampleTxtBox.Text.Length > 0 && testSampleTxtBox.Text.Length > 0)
                {
                    calculateBtn.IsEnabled = true;
                }
                else
                {
                    calculateBtn.IsEnabled = false;
                }
            }
            else if (calculateBtn != null)
            {
                if (batchOfFilesTxtBox.Text.Length > 0)
                {
                    calculateBtn.IsEnabled = true;
                }
                else
                {
                    calculateBtn.IsEnabled = false;
                }
            }
        }
        #endregion

        
    }
}
