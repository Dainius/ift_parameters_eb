﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Threading;
using System.Windows;
using System.Collections.Generic;

namespace FT_Parameters
{
    public class ServerConnection
    {
        private static readonly HttpClient client = new HttpClient();

        public string Link { get; set; }
        public string TokenStaticPart { get; set; }
        public string Token { get; set; }
        public string Timezone { get; set; }
        public string ResponseString { get; set; }

        public string AuthenticationService { get; set; }
        public string CredentialsString { get; set; }


        public bool DownloadStatus { get; set; }
        public static List<bool> DownloadComplete { get; set; }
        public int Timeout { get; set; }

        public ServerConnection()
        {
            Link = "https://track2.ruptela.com/download/excel/en-GB/history/";
            TokenStaticPart = "?token=";
            Timezone = "&timezone=Europe%2FVilnius";

            AuthenticationService = "https://track2.ruptela.com/gateway/authentication-service/v20180307/web-sessions";
            CredentialsString = @"{""username"": ""USERNAMEPH"", ""password"": ""PASSWORDPH"", ""remember"": ""false"", ""serviceProviderId"": ""1""}".Replace("USERNAMEPH", Credentials.Username).Replace("PASSWORDPH", Credentials.Password);

            DownloadComplete = new List<bool>();
        }

        public async Task Authorise()
        {
            var response = await client.PostAsync(AuthenticationService, new StringContent(CredentialsString, Encoding.UTF8, "application/json"));
            ResponseString = await response.Content.ReadAsStringAsync();
        }

        public async void Download(string fileName, string objectID, string periodStart, string periodEnd, string path, int index)
        {
            string pattern = @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})";

            if (ResponseString != null && Regex.IsMatch(ResponseString, pattern))
            {
                SetToken(ResponseString);
                string remoteUri = $"{Link}{objectID}{periodStart}{periodEnd}{TokenStaticPart}{Token}{Timezone}";

                using (WebClient myWebClient = new WebClient())
                {
                    try
                    {
                        await Task.Run(() => myWebClient.DownloadFile(remoteUri, $"{path}/{fileName}.xlsx"));
                    }
                    catch (System.Net.WebException ex)
                    {
                        var statuscode = ((HttpWebResponse)ex.Response).StatusCode;
                        {
                            switch (statuscode)
                            {
                                case (HttpStatusCode.Forbidden):
                                    MessageBox.Show("Forbidden.", "Systen.NET WebException ");
                                    break;
                                case (HttpStatusCode.NotFound):
                                    MessageBox.Show("File NotFound", "Systen.NET WebException ");
                                    break;

                                case (HttpStatusCode.GatewayTimeout):
                                    MessageBox.Show("GatewayTimeout.", "Systen.NET WebException ");
                                    break;

                                case (HttpStatusCode.ServiceUnavailable):
                                    MessageBox.Show("ServiceUnavailable.", "Systen.NET WebException ");
                                    break;
                                case (HttpStatusCode.BadRequest):
                                    MessageBox.Show("BadRequest.", "Systen.NET WebException ");
                                    break;
                                default: throw;
                            }
                        }

                        DownloadComplete[index] = true;
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show(fileName + ": Could not get response string.");
            }
            DownloadComplete[index] = true;

            if (MainWindow.StopDownloading == true)
            {
                MainWindow.DownloadingStopped = true;
            }
        }

        private void SetToken(string responseString)
        {
            string pattern = @"(\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12})";

            Regex tokenRgx = new Regex(pattern);
            Match tokenMch = tokenRgx.Match(responseString);
            Token = tokenMch.Groups[0].Value;
        } 

        public void LocalStatusUpdate(string fileName, string path, Files downloadedFile, int index)
        {
            string downloadStatus = String.Empty;
            Thread.Sleep(1000);
            GridStatusUpdate(downloadedFile, downloadStatus);

            try
            {
                if (Directory.Exists(path))
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(path);
                    foreach (FileInfo file in di.EnumerateFiles())
                    {
                        Console.WriteLine(file.Name);/////////
                        if (file.Name.Contains(fileName) && file.Name.Contains("xlsx"))
                        {
                            Console.WriteLine("IF");//////////////
                            int timeout = 0;
                            while (timeout < 60)
                            {
                                Console.WriteLine(fileName + " index: " + index);
                                if (DownloadComplete[index] == true)
                                {
                                    file.Refresh();
                                    downloadStatus = $"       Done       {file.Length / 1024 + 1} KB";
                                    GridStatusUpdate(downloadedFile, downloadStatus);
                                    break;
                                }

                                file.Refresh();
                                downloadStatus = $"Downloading       {file.Length / 1024 + 1} KB";
                                GridStatusUpdate(downloadedFile, downloadStatus);
                                Thread.Sleep(1000);
                                timeout++;

                                if (timeout >= 60)
                                {
                                    downloadStatus = $"Timeout of {60}s reached";
                                    GridStatusUpdate(downloadedFile, downloadStatus);
                                }
                                file.Refresh();
                                Console.WriteLine(fileName + " ds: " + downloadStatus);////////
                                Console.WriteLine(fileName + " tmo: " + timeout);///////////   
                            }
                            break;
                        }
                        else
                        {
                            downloadStatus = "Download failed";
                            GridStatusUpdate(downloadedFile, downloadStatus);
                        }
                    }
                }
                GridStatusUpdate(downloadedFile, downloadStatus);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void GridStatusUpdate(Files downloadedFile, string downloadStatus)
        {
            try
            {
                System.Action action = () => MainWindow.CurrentForm.DtGridViewRefresh();
                downloadedFile.Status = downloadStatus;
                MainWindow.CurrentForm.Dispatcher.BeginInvoke(action);
            }
            catch (Exception ex)
            {

                MessageBox.Show($"Data Grid update for {downloadedFile.DeviceName}: {ex.Message}");
            } 
        }
    }
}
